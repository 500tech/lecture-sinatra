module SampleHelpers
	
  LINKS = {
  	:home     => '/',
    :form     => '/form',
  	:json     => '/json',
  	:file     => '/file',
    :new_user => '/users/new',
  }

	def link(target)
		"<a href='#{path(target)}'>#{target}</a>"
	end

	def path(target)
		LINKS[target] || "/#{target}"
	end
end
