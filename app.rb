require 'sinatra'
require 'json'
require 'haml'
require 'sass'
require 'sinatra/static_assets'
require 'sinatra/activerecord'
require 'redis'
require "sinatra/reloader" if development?

set :database, 'sqlite:///sample.db'

$REDIS = Redis.new

configure do
  enable :sessions
end

require_relative 'helpers/sample_helpers'
require_relative 'models/user'

helpers SampleHelpers


post '/create' do
  redirect "/#{params[:name]}"
end

get '/form' do
  haml :form
end

get '/file' do
	content_type :txt
	attachment 'app.rb'
end

get '/json' do
	content_type :json
  { :users => User.all }.to_json
end

get '/style.css' do
  sass :'stylesheets/style'
end

get '/' do
  haml :index
end

require_relative 'controllers/users'
