require 'open-uri'
require_relative 'user/etoro_data'

class User < ActiveRecord::Base
  include EtoroData

  validates_presence_of :name
  validates :age, :numericality => { :greater_than_or_equal_to => 18 }
end
