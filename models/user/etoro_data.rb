module EtoroData
  def image
    remote_data['BasicDetails']['Avatars']['Large'] rescue ''
  end

  def about
    remote_data['BasicDetails']['AboutMe'] rescue ''
  end

  def followers
    data = remote_data['SocialGraphPreview']['Followers']['Users'][0..5] rescue []

    data.map do |user| 
      { :name    => user['Username'], 
        :photo   => user['Avatars']['Small'],
        :country => user['Country']['Name']  } 
    end
  end

  private

  def remote_data
    @data ||= fetch_from_redis || fetch_from_url
  end

  def fetch_from_redis
    (str = $REDIS.get(name)) ? JSON.parse(str) : nil
  end

  def fetch_from_url
    file = open("https://openbook.etoro.com/api/users/preview/#{name}")

    if file and file.content_type == "application/json"
      data = file.read
      $REDIS.set(name, data)
      JSON.parse(data)
    else
      nil
    end
  end
end
