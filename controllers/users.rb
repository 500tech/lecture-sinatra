get '/users' do
  @users = User.all 
  haml :index
end

get "/users/new" do
  @user = User.new
  haml :"users/new"
end

get '/users/:id' do
  @user = User.find(params[:id])
  haml :'users/show'
end

post "/users" do
  @user = User.new(params[:user])

  if @user.save
    redirect "/"
  else
    haml :"users/new"
  end
end

get '/users/:id/edit' do
  @user = User.find(params[:id])
  haml :'users/form'
end

put '/users/:id' do
  @user = User.find(params[:id])

  if @user.update_attributes(params[:user])
    redirect "/"
  else
    haml :"users/edit"
  end
end
